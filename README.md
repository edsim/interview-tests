# Technical interview
## Coding tests

As preparation for the interview we would ask you do to a small coding exercise which will form the basis of our discussion. It is composed of two parts, each described in a separate Jupyter notebook attached. 
You should not use more than 2-3 hours to solve the exercises. Please return the solution to GOPAM@orsted.dk, Cc EDSIM@orsted.dk in a zipped folder, or send us a link to e.g. your GitHub repo, at least 24 hours before your interview date.
Should you have any questions, feel free to use the above mentioned email addresses to reach out, or call Edoardo at +4599556455.

